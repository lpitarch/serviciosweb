<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>Cliente sin WSDL</title>
		<meta name="description" content="">
		<meta name="author" content="">

		<meta name="viewport" content="width=device-width; initial-scale=1.0">

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link rel="shortcut icon" href="/favicon.ico">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
		<style>
			#container{
				font-family:Arial, Helvetica, sans-serif;
				width:40%;
				margin:0 auto;
				border:1px dotted black;
				padding:20px;
			}
			#container a{
				color:red;
				text-decoration:none;
			}
			#container a:hover{
				text-decoration:underline;
			}
		</style>
	</head>

	<body>
		<div id="container">
			<h1>Cliente sin WSDL</h1>
<?php
	
	$url="http://localhost/ejerciciosphp/serviciosweb/servicio.php";
	$uri="http://localhost/ejerciciosphp/serviciosweb/";
	$cliente = new SoapClient(null,array('location'=>$url,'uri'=>$uri));
	
if($_POST['enviar']){
		
		if($_POST['familias']!=null){
			echo "Todas las familias: <br>";
			$datos = $cliente->getFamilias();
			foreach ($datos as $key) {
				echo $key." <br>";
			}
		echo "<br>";
		}
		
		if($_POST['productostv']!=null){
			echo "Todos los productos de TV: <br>";
			$datos = $cliente->getProductosFamilia("TV");
			foreach ($datos as $key) {
				echo $key." <br>";
			}
		echo "<br>";
		}
	
		if($_POST['stock']!=""){
			
			$datos = $cliente->getStock($_POST['stock'],$_POST['tienda']);
			if($datos==""){
				echo "No existe stock del producto ".$_POST['stock']." en esta tienda<br>";	
			}else{
				echo "El stock del producto ".$_POST['stock'] ." en ";
				if($_POST['tienda']==1){
					echo "la central ";
				}
				if($_POST['tienda']==2){
					echo "la sucursal 1 ";
				}
				if($_POST['tienda']==3){
					echo "la sucursal 2 ";
				}
				echo "es de ".$datos." unidades<br>";
			}
		}
		
		if($_POST['precio']!=""){
			echo "<br>";
			$datos = $cliente->getPVP($_POST['precio']);
			echo "El precio del producto ".$_POST['precio']." es de ".$datos ." euros";
		}
	?>
	<br>
	<br>
	<a href="clientew.php">Probar otra vez</a>
	<?php
	}else{
		?>
			<form action="clientew.php" method="post">
				<table>
					<tr>
						<td>Obtener familias?</td>
						<td><input type="checkbox" name="familias" />
					</tr>
					<tr>
						<td>Obtener productos de TV?</td>
						<td><input type="checkbox" name="productostv" />
					</tr>
					<tr>
						<td>Ver Stock:</td>
						<td><select name="stock">
							<option value="3DSNG">Nintendo 3DS Negra</option>
							<option value="EEEPC1005PXD">Asus EEEPC Intel</option>
							<option value="PIXMAIP4850">Canon Pixma IP4850</option>
							<option value="TSSD16GBC10J">Toshiba SD16GB Class10</option>
							<option value="LGM237WDP">LG TDT HD 23 M237WDP-PC FULL HD</option>
							<option value="PS3320GB">PS3 con disco duro de 320GB</option>
						</select>
						Elige tienda:
						<select name="tienda">
							<option value="1">Central</option>
							<option value="2">Tienda 1</option>
							<option value="3">Tienda 2</option>
						</select>
						</td>
					</tr>
					<tr>
						<td>Ver Precio</td>
						<td>
						<select name="precio">
							<option value="3DSNG">Nintendo 3DS Negra</option>
							<option value="EEEPC1005PXD">Asus EEEPC Intel</option>
							<option value="PIXMAIP4850">Canon Pixma IP4850</option>
							<option value="TSSD16GBC10J">Toshiba SD16GB Class10</option>
							<option value="LGM237WDP">LG TDT HD 23 M237WDP-PC FULL HD</option>
							<option value="PS3320GB">PS3 con disco duro de 320GB</option>
						</select></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="Enviar" name="enviar"></td>
					</tr>
				</table>
			</form>
		<?php
	}
?>		</div>
	</body>
</html>