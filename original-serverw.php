<?php
	require_once('DB.php');
	require_once('Producto.php');
	class serverw{
		/**
	     * Obtener el precio.
	     * @param string $cod_producto
	     * @return float
	     */
		public function getPVP($cod_producto){
			$producto= DB::obtieneProducto($cod_producto);
			return $producto->getPVP();
		}
		/**
	     * Obtener el stock.
	     * @param string $cod_producto
	     * @param int $tienda
	     * @return int
	     */
		public function getStock($cod_producto,$tienda){
			return DB::obtieneStock($cod_producto,$tienda);
		}
		/**
	     * Obtener las familias.
	     * @return string[]
	     */
		public function getFamilias(){
			$dato= DB::obtieneFamilias();
			return $dato;
		}
		/**
		 * Obtener los productos de una familia.
	     * @param string $familia
	     * @return string[]
	     */
		public function getProductosFamilia($familia){
			return DB::obtieneProductosFamilia($familia);
		}
	}
?>