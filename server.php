<?php

	require_once('DB.php');
	
	class server{
		
		
		public function getPVP($cod_producto){
			$producto= DB::obtieneProducto($cod_producto);
			return $producto->getPVP();
		}
		
		public function getStock($cod_producto,$tienda){
			return DB::obtieneStock($cod_producto,$tienda);
		}
		
		public function getFamilias(){
			$dato= DB::obtieneFamilias();
			return $dato;
		}
		
		public function getProductosFamilia($familia){
			return DB::obtieneProductosFamilia($familia);
		}
		
		
	}
	
?>