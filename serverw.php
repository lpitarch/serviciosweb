<?php

/**
 * serverw class
 * 
 *  
 * 
 * @author    {author}
 * @copyright {copyright}
 * @package   {package}
 */
class serverw extends \SoapClient {

  const WSDL_FILE = "http://localhost/ejerciciosphp/serviciosweb/serviciow.wsdl";
  private $classmap = array(
                                   );

  public function __construct($wsdl = null, $options = array()) {
    foreach($this->classmap as $key => $value) {
      if(!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }
    if(isset($options['headers'])) {
      $this->__setSoapHeaders($options['headers']);
    }
    parent::__construct($wsdl ?: self::WSDL_FILE, $options);
  }

  /**
   * Obtener el precio. 
   *
   * @param string $cod_producto
   * @return float
   */
  public function getPVP($cod_producto) {
  	
    return $this->__soapCall('getPVP', array($cod_producto),       array(
            'uri' => 'http://localhost/ejerciciosphp/serviciosweb',
            'soapaction' => ''
           )
      );
  }

  /**
   * Obtener el stock. 
   *
   * @param string $cod_producto
   * @param int $tienda
   * @return int
   */
  public function getStock($cod_producto, $tienda) {
    return $this->__soapCall('getStock', array($cod_producto, $tienda),       array(
            'uri' => 'http://localhost/ejerciciosphp/serviciosweb',
            'soapaction' => ''
           )
      );
  }

  /**
   * Obtener las familias. 
   *
   * @param 
   * @return stringArray
   */
  public function getFamilias() {
    return $this->__soapCall('getFamilias', array(),       array(
            'uri' => 'http://localhost/ejerciciosphp/serviciosweb',
            'soapaction' => ''
           )
      );
  }

  /**
   * Obtener los productos de una familia. 
   *
   * @param string $familia
   * @return stringArray
   */
  public function getProductosFamilia($familia) {
    return $this->__soapCall('getProductosFamilia', array($familia),       array(
            'uri' => 'http://localhost/ejerciciosphp/serviciosweb',
            'soapaction' => ''
           )
      );
  }

}

?>
